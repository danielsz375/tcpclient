package com.company.RequestChainOfResponsibility;

import com.company.Message.MessageEnum;
import com.company.Message.Request;
import com.company.Message.Response;
import com.company.Screenshot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class BitmapRequestHandler extends RequestHandler {
    @Override
    public void handleRequest(Request request, ObjectOutputStream objectOutputStream) {
        if(request.getRequestCode() == MessageEnum.REQUEST_BMP) {
            try {
                Screenshot screenshot = new Screenshot();
                System.out.println("Wysyłanie screenshota...");
                byte[] screenshotAsBytes = screenshot.getScreenshotAsBytesArray();
                Response responseToSend = new Response(MessageEnum.RESPONSE_BMP, screenshotAsBytes);
                objectOutputStream.writeObject(responseToSend);
                System.out.println("Wysłano screenshot!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            this.getNextHandler().handleRequest(request, objectOutputStream);
        }
    }
}
