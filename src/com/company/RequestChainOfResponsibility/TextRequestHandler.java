package com.company.RequestChainOfResponsibility;

import com.company.Message.MessageEnum;
import com.company.Message.Request;
import com.company.Message.Response;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class TextRequestHandler extends RequestHandler {
    @Override
    public void handleRequest(Request request, ObjectOutputStream objectOutputStream) {
        if(request.getRequestCode() == MessageEnum.REQUEST_TXT) {
            try {
                //tymczasowe rozwiązanie, należy to zastąpić wzorcem State
                Scanner scanner = new Scanner(System.in);
                System.out.println("Wpisz jakiś tekst:");
                String contentToPack = scanner.nextLine();

                System.out.println("Wysyłanie tekstu...");
                byte[] contentInBytes = contentToPack.getBytes();
                Response responseToSend = new Response(MessageEnum.RESPONSE_TXT, contentInBytes);
                objectOutputStream.writeObject(responseToSend);
                System.out.println("Wysłano wczytany tekst!");

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            this.getNextHandler().handleRequest(request, objectOutputStream);
        }
    }
}
