package com.company.RequestChainOfResponsibility;

import com.company.Message.Request;

import java.io.ObjectOutputStream;

public abstract class RequestHandler {
    private RequestHandler nextHandler;

    public void setNextHandler(RequestHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public abstract void handleRequest(Request request, ObjectOutputStream objectOutputStream);

    public RequestHandler getNextHandler() {
        return this.nextHandler;
    }
}
