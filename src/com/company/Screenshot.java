package com.company;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Screenshot {
    private BufferedImage screenFullImage;

    public Screenshot() {
        try {
            Robot robot = new Robot();
            String format = "bmp";
            String fileName = "screenshot." + format;

            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            this.screenFullImage = robot.createScreenCapture(screenRect);
            ImageIO.write(screenFullImage, format, new File(fileName));
        } catch (AWTException | IOException ex) {
            System.err.println(ex);
        }

        System.out.println("Screenshot został wykonany!");
    }

    public byte[] getScreenshotAsBytesArray() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.flush();
            ImageIO.write(screenFullImage, "bmp", baos);
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            return imageInByte;
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Nie wiem co z tym zrobić
        return null;
    }
}
