package com.company.KeyloggerState;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class KeyloggerThread extends Thread {
    private boolean doStop = false;
    private List<String> textsList = new ArrayList<>();

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while(this.keepRunning()) {
            String text = scanner.nextLine();
            this.textsList.add(text);
        }
    }

    public synchronized void doStop() {
        this.doStop = true;
    }

    private synchronized boolean keepRunning() {
        return this.doStop == false;
    }

    public List<String> getTextsList() {
        return this.textsList;
    }
}
